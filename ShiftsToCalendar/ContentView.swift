//
//  ContentView.swift
//  ShiftsToCalendar
//
//  Created by Jose Antonio Mendoza on 8/3/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationStack {
            Text("Semanal")
                .font(.largeTitle)
                .bold()
                .padding(.top, 8)
            MainView()
        }
        .frame(maxWidth: 220, maxHeight: 280)
    }
}

struct MainView: View {
    @State private var filename: URL?
    @State private var showFileChooser = false
    @State private var agent: Agent = Agent.agents[1]
    
    @StateObject var shiftsViewModel = ShiftsViewModel()
    
    var body: some View {
        VStack {
            VStack {
                Picker("Agente", selection: $agent) {
                    ForEach(Agent.agents, id: \.cf) { agent in
                        Text("\(agent.cf)").tag(agent)
                    }
                }
                
                LabeledContent("Categoría", value: agent.category.rawValue)
                LabeledContent("Residencia", value: agent.location.rawValue.capitalized)
                
//                Picker("Categoría", selection: $category) {
//                    ForEach(Category.allCases, id: \.self) { category in
//                        Text("\(category.rawValue)").tag(category)
//                    }
//                }
                
//                Picker("Residencia", selection: $location) {
//                    ForEach(Location.allCases, id: \.self) { location in
//                        Text("\(location.rawValue.capitalized)").tag(location)
//                    }
//                }
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .padding(.horizontal, 16)
            
            VStack {
                Text(filename?.lastPathComponent ?? "")
                Button("Select file") {
                    let panel = NSOpenPanel()
                    panel.allowsMultipleSelection = false
                    panel.canChooseDirectories = false
                    if panel.runModal() == .OK {
                        self.filename = panel.url
                    }
                }
            }
            .padding(.bottom, 8)
            
            Button("Get Week Plan") {
                if let filename {
                    Task {
                        await shiftsViewModel.populateShifts(category: agent.category, location: agent.location)
                        
                        let fileManager = FileManager(
                            filepath: filename.relativePath,
                            agent: agent,
                            shifts: shiftsViewModel.shifts
                        )
                        
                        try await fileManager.run()
                        
                        guard let weekPlan = fileManager.agentWeekPlan else { return }
                        
                        let calendarManager = CalendarManager()
                        calendarManager.insertEvents(weekPlan: weekPlan)
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
