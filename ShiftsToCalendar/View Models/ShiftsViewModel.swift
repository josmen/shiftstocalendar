//
//  ShiftsViewModel.swift
//  ShiftsToCalendar
//
//  Created by Jose Antonio Mendoza on 10/3/23.
//

import Foundation

class ShiftsViewModel: ObservableObject {
    @Published var shifts = [Shift]()
    
    @MainActor
    func populateShifts(category: Category, location: Location) async {
        guard let url = URL(string: "https://shifts-api.josmen.synology.me/api/shifts/actual?category=\(category)&location=\(location)") else {
            return
        }
        
        do {
            let shifts = try await ApiService().getShifts(url: url)
            self.shifts = shifts
        } catch {
            print("❌ Error: \(error)")
        }
    }
}
