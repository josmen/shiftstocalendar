//
//  Agent.swift
//  ShiftsToCalendar
//
//  Created by Jose Antonio Mendoza on 11/3/23.
//

import Foundation

enum Category: String, CaseIterable {
    case maquinista = "Maquinista"
    case usi = "USI"
}

enum Location: String, CaseIterable {
    case benidorm
    case denia
}

struct Agent: Hashable {
    let cf: Int
    let category: Category
    let location: Location
    
    public static var agents: [Agent] = [
        Agent(cf: 1508, category: .usi, location: .benidorm),
        Agent(cf: 2076, category: .maquinista, location: .benidorm)
    ]
}


