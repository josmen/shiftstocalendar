import Foundation

struct WeekPlan: Codable {
    let agent: Int
    let week: [WorkDay]
}

struct WorkDay: Codable {
    let shift: Shift
    let date: Date
}

struct Shift: Codable {
    let name: String
    let start: String
    let end: String
}

struct Event {
    let title: String
    let startDate: Date
    let endDate: Date
}

extension WorkDay {
    func convertToEvent() -> Event {
        let startHour = Int(self.shift.start.prefix(2))
        let startMins = Int(self.shift.start.suffix(2))
        let endHour = Int(self.shift.end.prefix(2))
        let endMins = Int(self.shift.end.suffix(2))
        
        let startDate = Calendar.current.date(bySettingHour: startHour!, minute: startMins!, second: 0, of: self.date)!
        let endDate = Calendar.current.date(bySettingHour: endHour!, minute: endMins!, second: 0, of: self.date)!
        
        return Event(title: "Turno \(self.shift.name)", startDate: startDate, endDate: endDate)
    }
}
