//
//  NetworkManager.swift
//  ShiftsToCalendar
//
//  Created by Jose Antonio Mendoza on 9/3/23.
//

import Foundation

enum NetworkError: Error {
    case invalidResponse
}

class ApiService {

    func getShifts(url: URL) async throws -> [Shift] {
        let (data, response) = try await URLSession.shared.data(from: url)
        
        guard let httpResponse = response as? HTTPURLResponse,
              httpResponse.statusCode == 200 else {
            throw NetworkError.invalidResponse
        }
        
        return try JSONDecoder().decode([Shift].self, from: data)
    }
}
