//
//  ShiftsToCalendarApp.swift
//  ShiftsToCalendar
//
//  Created by Jose Antonio Mendoza on 8/3/23.
//

import SwiftUI

@main
struct ShiftsToCalendarApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
