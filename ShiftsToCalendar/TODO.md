#  TODO

## NetworkManager

- Modificar la función fetch para pasarle la url, de tal modo que según la entrada de datos se pase la url de usis o de maquinista

## FileManager

- Borrar la función fetchShifts de aquí y usar la de NetworkManager

- Volver a ver el vídeo de Azamsharp "Building macOS Menu Bar Stocks App Using SwiftUI" para coger ideas. 



- Al pulsar el boton "Get Week Plan" pasar a una nueva pantalla con la lista de los eventos y la posibilidad de añadir nuevos o modificar los actuales, y un nuevo botón para crear los eventos en el calendario.
