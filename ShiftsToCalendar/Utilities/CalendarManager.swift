//
//  CalendarManager.swift
//  ShiftsToCalendar
//
//  Created by Jose Antonio Mendoza on 9/3/23.
//

import EventKit
import Foundation

enum CalendarName: String {
    case jose = "Trabajo Jose"
    case itziar = "Trabajo Itziar"
}

struct CalendarManager {
    
    private func checkPermission(store: EKEventStore) -> Bool {
        var grantedAcces = false

        switch EKEventStore.authorizationStatus(for: .event) {
        case .notDetermined:
            store.requestAccess(to: .event) { status, error in
                if let error {
                    print("❌ Error requesting access: \(error.localizedDescription)")
                } else if status {
                    grantedAcces = true
                }
            }
        case .restricted:
            print("❌ Error: restricted")
            grantedAcces = false
        case .denied:
            print("❌ Error: denied")
            grantedAcces = false
        case .authorized:
            grantedAcces = true
        @unknown default:
            print("❌ Error: unknown")
            grantedAcces = false
        }
        
        return grantedAcces
    }
    
    func insertEvents(weekPlan: WeekPlan) {
        let eventStore = EKEventStore()
        
        guard checkPermission(store: eventStore) else {
            return
        }
        
        let calendarName: CalendarName = weekPlan.agent == 1508 ? .itziar : .jose
        
        let calendars = eventStore.calendars(for: .event)
        
        let events = weekPlan.week.map { $0.convertToEvent() }
        
        for calendar in calendars {
            if calendar.title == calendarName.rawValue {
                for event in events {
                    let calendarEvent = EKEvent(eventStore: eventStore)
                    calendarEvent.calendar = calendar
                    calendarEvent.title = event.title
                    calendarEvent.startDate = event.startDate
                    calendarEvent.endDate = event.endDate
                    let reminder1 = EKAlarm(relativeOffset: -60 * 60 * 24) // 1 day before event
                    let reminder2 = EKAlarm(relativeOffset: -60 * 60) // 1 hour before event
                    calendarEvent.alarms = [reminder1, reminder2]
                    
                    do {
                        try eventStore.save(calendarEvent, span: .thisEvent)
                        print("Event saved!")
                    } catch {
                        print(error.localizedDescription)
                    }
                }
            }
        }
    }
}
